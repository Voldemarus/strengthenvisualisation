//
//  Preferences.m
//  Portal Picture
//
//  Created by Водолазкий В.В. on 30.12.14.
//  Copyright (c) 2014 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import "Preferences.h"

@interface Preferences () {
	NSUserDefaults *prefs;
}

@end

NSString * const VVVWakeupChanged		=	@"VVVWakeupChanged";
NSString * const VVVForegroundChanged	=	@"VVVForegroundChanged";
NSString * const VVVautoWakeupChanged	=	@"VVVautoWakeupChanged";

NSString * const VVVwakeup				=	@"VVVwakeup";
NSString * const VVVforeground			=	@"VVVforeground";
NSString * const VVVautowakeup			=	@"VVVautowakeup";
NSString * const VVVfirstStart			=	@"VVVfirstStart";
NSString * const VVVpreselectedId		=	@"VVVpreselectedId";

@implementation Preferences

+ (Preferences *) sharedPreferences
{
	static Preferences *_Preferences;
	if (_Preferences == nil) {
		_Preferences = [[Preferences alloc] init];
	}
	return _Preferences;
}

//
// Init set of data for case when actual preference file is not created yet
//
+ (void)initialize
{
	NSMutableDictionary  *defaultValues = [NSMutableDictionary dictionary];
	// set up default parameters
	[defaultValues setObject:@(YES) forKey:VVVforeground];
	[defaultValues setObject:@(YES) forKey:VVVautowakeup];
	[defaultValues setObject:@(YES) forKey:VVVfirstStart];
	
	[[NSUserDefaults standardUserDefaults] registerDefaults: defaultValues];
	
}

- (id) init
{
	if (self = [super init]) {
		prefs = [NSUserDefaults standardUserDefaults];
		
		// Set up default autowake time. Issue 11.
		NSCalendar *cal = [NSCalendar currentCalendar];
		NSDateComponents *startComponents = [cal components:VVVallComponents fromDate:
											 [NSDate dateWithTimeIntervalSinceReferenceDate:0]];
		[startComponents setHour:10];
		[startComponents setMinute:0];
		[startComponents setSecond:0];
		
		NSDate *startDate = [cal dateFromComponents:startComponents];
		self.wakeupTime = startDate;

	}
	return self;
}


- (void) flush
{
	[[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark -


- (NSDate *) wakeupTime
{
	return (NSDate *)[prefs objectForKey:VVVwakeup];
}

- (void) setWakeupTime:(NSDate *)wakeupTime
{
	if ([wakeupTime isEqualToDate:self.wakeupTime]) return;
	[prefs setObject:wakeupTime forKey:VVVwakeup];
	[[NSNotificationCenter defaultCenter] postNotificationName:VVVWakeupChanged object:wakeupTime];
}


- (BOOL) alwaysForeGround
{
	return [prefs boolForKey:VVVforeground];
}

- (void) setAlwaysForeGround:(BOOL)alwaysForeGround
{
	if (alwaysForeGround == self.alwaysForeGround) return;
	[prefs setBool:alwaysForeGround forKey:VVVforeground];
	[[NSNotificationCenter defaultCenter] postNotificationName:VVVForegroundChanged object:@(alwaysForeGround)];
}

- (BOOL) autoWakeup
{
	return [prefs boolForKey:VVVautowakeup];
}

- (void) setAutoWakeup:(BOOL)autoWakeup
{
	if (autoWakeup == self.autoWakeup) return;
	[prefs setBool:autoWakeup forKey:VVVautowakeup];
	[[NSNotificationCenter defaultCenter] postNotificationName:VVVautoWakeupChanged object:@(autoWakeup)];
}

- (BOOL) firstStart
{
	return [prefs boolForKey:VVVfirstStart];
}

- (void) setFirstStart:(BOOL)firstStart
{
	[prefs setBool:firstStart forKey:VVVfirstStart];
}

- (NSString *) preselectedSetId
{
	return [prefs objectForKey:VVVpreselectedId];
}

- (void) setPreselectedSetId:(NSString *)preselectedSetId
{
	[prefs setObject:preselectedSetId forKey:VVVpreselectedId];
}

@end
