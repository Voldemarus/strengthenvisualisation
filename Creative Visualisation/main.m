//
//  main.m
//  Creative Visualisation
//
//  Created by Водолазкий В.В. on 23.12.14.
//  Copyright (c) 2014 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
	return NSApplicationMain(argc, argv);
}
