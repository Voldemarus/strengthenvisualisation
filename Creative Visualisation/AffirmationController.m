//
//  AffirmationController.m
//  Creative Visualisation
//
//  Created by Водолазкий В.В. on 07.02.15.
//  Copyright (c) 2015 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import "AffirmationController.h"

@implementation AffirmationController

- (void)awakeFromNib

{
	
	NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"id"
														 ascending:YES];
	[self setSortDescriptors:[NSArray arrayWithObject:sort]];
	[super awakeFromNib];
}

- (void)remove:(id)sender

{
	[super remove:sender];
	[self reindexEntries];
	
}

- (void)insertObject:(id)object atArrangedObjectIndex:(NSUInteger)index

{
	[object setValue:[NSNumber numberWithLongLong:index] forKey:@"id"];
	[super insertObject:object atArrangedObjectIndex:index];
	[self reindexEntries];
	
}

- (void) reindexEntries

{
	
	// Note: use a temporary array since modifying an item in arrangedObjects
	//       directly will cause the sort to trigger thus throwing off
	//       the re-indexing.
	
	NSInteger count = [[self arrangedObjects] count];
	NSArray *tmpArray = [NSArray arrayWithArray:[self arrangedObjects]];
	for(int ndx = 0; ndx < count ; ndx++){
		id entry = [tmpArray objectAtIndex:ndx];
		[entry setValue:[NSNumber numberWithInt:ndx] forKey:@"id"];
	}
}


- (NSArray *)arrangeObjects:(NSArray *)objects

{
	
	// Note: at this point the data objects are CoreData faults and thus contain
	//       no real data. So, go ahead and batch fault (load) the data for use
	//       in sorting
	
	
	NSError *error = nil;
	
	NSManagedObjectContext *moc = [self managedObjectContext];
	NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Affirmation"];
	[request setReturnsObjectsAsFaults:NO];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self IN %@", objects];
	[request setPredicate:predicate];
	[request setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"id" ascending:YES]]];
	NSArray *result = [moc executeFetchRequest:request error:&error];
	
	// NSArray *arranged = [super arrangeObjects:objects];
	return result;
	
}

@end
