//
//  AppDelegate.h
//  Creative Visualisation
//
//  Created by Водолазкий В.В. on 23.12.14.
//  Copyright (c) 2014 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SlideShowWindowController.h"
#import "AffirmationController.h"

extern NSString * const VVVStopApplication;

@interface AppDelegate : NSObject <NSApplicationDelegate,  NSTextFieldDelegate, NSComboBoxDataSource> {
	SlideShowWindowController *slideShow;
	
	__weak IBOutlet NSTableView *tableView;
	__weak IBOutlet NSPopover *timeSelectPopover;
	
}

@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) IBOutlet NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) IBOutlet NSArrayController *setArrayController;
@property (nonatomic, strong) IBOutlet AffirmationController  *affirmationController;
@property (weak) IBOutlet NSButton *folderNameButton;


@property (weak) IBOutlet NSButton *autoWakeupCheckbox;
- (IBAction)autoWakeupCheckboxChanged:(id)sender;

@property (weak) IBOutlet NSDatePicker *timePicker;
@property (weak) IBOutlet NSDatePicker *graphicalTimePicker;

- (IBAction)wakeTimeChanged:(id)sender;

- (Set *) getNextSet;

@property (weak) IBOutlet NSButton *foregroundCheckbox;

- (NSArray *) affirmationSortDescriptior;

- (IBAction) openSettingswindow:(id)sender;

- (IBAction)foregroundCheckboxChanged:(id)sender;

- (IBAction)timeSetButtonPressed:(id)sender;

- (IBAction)selectFolderId:(id)sender;

- (IBAction) playFolderSet:(id)sender;

- (IBAction)restoreDataBaseToDefaultState:(id)sender;

@end










