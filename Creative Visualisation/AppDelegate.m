//
//  AppDelegate.m
//  Creative Visualisation
//
//  Created by Водолазкий В.В. on 23.12.14.
//  Copyright (c) 2014 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import "AppDelegate.h"
#import "Set.h"
#import "Affirmation.h"

#import "Preferences.h"

NSString * const VVVStopApplication			=	@"VVVStopApplication";

@interface AppDelegate () {
	Preferences *prefs;
	
	NSTimer *wakeupTimer;
}

@property (weak) IBOutlet NSWindow *window;
- (IBAction)saveAction:(id)sender;

@end

@implementation AppDelegate

//
// This method checks, if there isthe application with same Bundle identifier loaded. If yes
// application is terminated immediately.
//
- (void)applicationWillFinishLaunching:(NSNotification *)aNotification
{
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *bundleId = mainBundle.bundleIdentifier;
	NSArray *running = [NSRunningApplication runningApplicationsWithBundleIdentifier:bundleId];
	if (running && [running count] > 1) {
		[[NSApplication sharedApplication] terminate:nil];
	}
	
}




- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	// initialize MOC to save time later
	NSManagedObjectContext *moc = self.managedObjectContext;
#pragma unused (moc)
	
	prefs = [Preferences sharedPreferences];
	
	
	self.affirmationController.sortDescriptors = [self affirmationSortDescriptior];
	[self.affirmationController rearrangeObjects];
	
	self.setArrayController.sortDescriptors = [self setSortDescriptor];
	[self.setArrayController rearrangeObjects];
	//
	// Set up global controls
	//
	self.autoWakeupCheckbox.state = prefs.autoWakeup;
	self.foregroundCheckbox.state = prefs.alwaysForeGround;
	
	self.timePicker.dateValue = prefs.wakeupTime;
	self.graphicalTimePicker.dateValue = prefs.wakeupTime;
	
	[self createDemoEntry];
	
	[self updateWakeupTimer];
	
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc addObserver:self selector:@selector(restoreMainWindow:) name:VVVrestoreSettingsScreen object:nil];
	// This observer allows to stop application with notificator sent from arbitrary control in application
	[nc addObserver:self selector:@selector(terminateNow:) name:VVVStopApplication object:nil];

	NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"setName" ascending:YES];
	[tableView setSortDescriptors:[NSArray arrayWithObject:sd]];
	
	
	//
	// Set up defaultView
	//
	if (prefs.firstStart) {
		// This is a first start, so we should show default window
		// and disable flag
		prefs.firstStart = NO;
		//
		// Nothing more - all wiil be processed automagically
	} else {
		// hide settings window, set up selected set nad play it automagically
		if (prefs.preselectedSetId) {
			Set *preselcted = [Set getSetById:prefs.preselectedSetId inMoc:moc];
			if (preselcted) {
				[self.setArrayController setSelectedObjects:@[preselcted]];
				[self playFolderSet:nil];
			}
		}
	}
	
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
	// Insert code here to tear down your application
	[self.managedObjectContext save:nil];
}


- (void) terminateNow:(NSNotification *) note
{
	[[NSApplication sharedApplication] terminate:nil];
}


#pragma mark - Core Data stack

@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize managedObjectContext = _managedObjectContext;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "geomatix.cz.Creative_Visualisation" in the user's Application Support directory.
    NSURL *appSupportURL = [[[NSFileManager defaultManager] URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    return [appSupportURL URLByAppendingPathComponent:@"geomatix.cz.Creative_Visualisation"];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel) {
        return _managedObjectModel;
    }
	
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Creative_Visualisation" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. (The directory for the store is created, if necessary.)
    if (_persistentStoreCoordinator) {
        return _persistentStoreCoordinator;
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *applicationDocumentsDirectory = [self applicationDocumentsDirectory];
    BOOL shouldFail = NO;
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    
    // Make sure the application files directory is there
    NSDictionary *properties = [applicationDocumentsDirectory resourceValuesForKeys:@[NSURLIsDirectoryKey] error:&error];
    if (properties) {
        if (![properties[NSURLIsDirectoryKey] boolValue]) {
            failureReason = [NSString stringWithFormat:@"Expected a folder to store application data, found a file (%@).", [applicationDocumentsDirectory path]];
            shouldFail = YES;
        }
    } else if ([error code] == NSFileReadNoSuchFileError) {
        error = nil;
        [fileManager createDirectoryAtPath:[applicationDocumentsDirectory path] withIntermediateDirectories:YES attributes:nil error:&error];
    }
    
    if (!shouldFail && !error) {
         NSURL *url = [applicationDocumentsDirectory URLByAppendingPathComponent:@"OSXCoreDataObjC.storedata"];
		NSLog(@"database path = %@",url.path);
		NSDictionary *optionsDirectory = @{NSMigratePersistentStoresAutomaticallyOption: @YES, NSInferMappingModelAutomaticallyOption: @YES};
		
		//
		// When application is started, in case when storage coordinator cannot open database,
		// old data has been removed and warehouse is created from scratch. This
		// approach allows to ignore all problems with migration. If application is started for the first time,
		// database file is not created yet.
		//
		NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
		if (![coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil
												 URL:url options:optionsDirectory error:&error]) {
			NSLog(@"Cannot connect database to coordinator object - %@",error);
			if ([fileManager fileExistsAtPath:[url path]]) {
				BOOL removed = [fileManager removeItemAtURL:url error:&error];
				if (!removed) {
					NSLog(@"Cannot clean up warehouse : error = %@",error);
				}
				else {
					NSLog(@"Old database file removed.");
					// try to recreate persistent coordinator from scratch
					error = nil;
					if (![coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:url options:optionsDirectory error:&error]) {
						NSLog(@"Cannot recreate coordinator - %@",error);
						[[NSApplication sharedApplication] presentError:error];
						return nil;
					}
				}
			} else {
				// Unknown error
				[[NSApplication sharedApplication] presentError:error];
				return nil;
			}
		}
        _persistentStoreCoordinator = coordinator;
    }
    
    if (shouldFail || error) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        if (error) {
            dict[NSUnderlyingErrorKey] = error;
        }
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        [[NSApplication sharedApplication] presentError:error];
    }
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];

    return _managedObjectContext;
}

#pragma mark - Core Data Saving and Undo support

- (IBAction)saveAction:(id)sender {
    // Performs the save action for the application, which is to send the save: message to the application's managed object context. Any encountered errors are presented to the user.
    if (![[self managedObjectContext] commitEditing]) {
        NSLog(@"%@:%@ unable to commit editing before saving", [self class], NSStringFromSelector(_cmd));
    }
    
    NSError *error = nil;
    if ([[self managedObjectContext] hasChanges] && ![[self managedObjectContext] save:&error]) {
        [[NSApplication sharedApplication] presentError:error];
    }
}

- (NSUndoManager *)windowWillReturnUndoManager:(NSWindow *)window {
    // Returns the NSUndoManager for the application. In this case, the manager returned is that of the managed object context for the application.
    return [[self managedObjectContext] undoManager];
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender {
    // Save changes in the application's managed object context before the application terminates.
    
    if (!_managedObjectContext) {
        return NSTerminateNow;
    }
    
    if (![[self managedObjectContext] commitEditing]) {
        NSLog(@"%@:%@ unable to commit editing to terminate", [self class], NSStringFromSelector(_cmd));
        return NSTerminateCancel;
    }
    
    if (![[self managedObjectContext] hasChanges]) {
        return NSTerminateNow;
    }
    
    NSError *error = nil;
    if (![[self managedObjectContext] save:&error]) {

        // Customize this code block to include application-specific recovery steps.              
        BOOL result = [sender presentError:error];
        if (result) {
            return NSTerminateCancel;
        }

        NSString *question = NSLocalizedString(@"Could not save changes while quitting. Quit anyway?", @"Quit without saves error question message");
        NSString *info = NSLocalizedString(@"Quitting now will lose any changes you have made since the last successful save", @"Quit without saves error question info");
        NSString *quitButton = NSLocalizedString(@"Quit anyway", @"Quit anyway button title");
        NSString *cancelButton = NSLocalizedString(@"Cancel", @"Cancel button title");
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:question];
        [alert setInformativeText:info];
        [alert addButtonWithTitle:quitButton];
        [alert addButtonWithTitle:cancelButton];

        NSInteger answer = [alert runModal];
        
        if (answer == NSAlertFirstButtonReturn) {
            return NSTerminateCancel;
        }
    }

    return NSTerminateNow;
}


- (IBAction)autoWakeupCheckboxChanged:(id)sender
{
	prefs.autoWakeup = self.autoWakeupCheckbox.state;
	[self updateWakeupTimer];
}


- (IBAction)foregroundCheckboxChanged:(id)sender
{
	prefs.alwaysForeGround = self.foregroundCheckbox.state;
}

//
// This selector is called both when direct click on fate picker and on TimeSet button
//

- (IBAction)timeSetButtonPressed:(id)sender
{
	[timeSelectPopover showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
}

- (IBAction)selectFolderId:(id)sender
{
	NSOpenPanel *open = [NSOpenPanel openPanel];
	open.canChooseDirectories = YES;
	open.canChooseFiles = NO;
	open.allowsMultipleSelection = NO;
	open.prompt = @"Select";

	NSString *startFolder = @"~/Documents";
	NSInteger selectionIndex = [self.setArrayController selectionIndex];
	Set *selected = nil;
	if (selectionIndex >= 0) {
		selected = (Set *)[[self.setArrayController arrangedObjects] objectAtIndex:selectionIndex];
	} else {
		NSBeep();
		return;
	}
	open.title = [NSString stringWithFormat:@"Image folder for set: \"%@\"",selected.setName];

	if ([selected setFolder]) {
		startFolder = [selected setFolder];
	} else {
		startFolder = [startFolder stringByExpandingTildeInPath];
	}

//	NSString *selectedDirectory = [startFolder lastPathComponent];
//	NSString *startingDirectory = [startFolder stringByDeletingLastPathComponent];
	NSURL *dir = [NSURL fileURLWithPath:startFolder];
	open.directoryURL = dir;
	
	[open beginSheetModalForWindow:self.window completionHandler:^(NSInteger returnCode)
	{
		if (returnCode == NSOKButton) {
			NSString *pathToFile = [[[open URLs] objectAtIndex:0] path];
			selected.setFolder = pathToFile;
		}
	}];
}

- (NSArray *) setSortDescriptor
{
	return [[NSArray alloc] initWithObjects:
			[[NSSortDescriptor alloc] initWithKey:@"setName" ascending:YES],
			nil];
}


- (NSArray *) affirmationSortDescriptior
{
	return [[NSArray alloc] initWithObjects:
			 [[NSSortDescriptor alloc] initWithKey:@"id" ascending:YES],
	nil];
}


#pragma mark -

- (void) createDemoEntry
{
	NSArray *defaultSetNames	=	@[@"Demo-Prosperity",
									  @"Demo-Relationships",
									  @"Demo-Health"];

	NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Set"];
	NSError *error = nil;
	NSInteger count = [self.managedObjectContext countForFetchRequest:request error:&error];
	if (error) {
		NSAlert *alert = [NSAlert alertWithError:error];
		[alert runModal];
		return;
	}
	if (count == 0) {
		// if no records in database at all, then we should create demo sets
		for (int i = 0; i <3; i++) {
			Set *newSet = [NSEntityDescription insertNewObjectForEntityForName:@"Set"
														inManagedObjectContext:self.managedObjectContext];
			if (newSet) {
				newSet.setName = [defaultSetNames objectAtIndex:i];
			}
		}
	}
}

- (Set *) getNextSet
{
	NSInteger selectionIndex = [self.setArrayController selectionIndex];
	if (selectionIndex >= 0) {
		selectionIndex++;
		if (selectionIndex >= [self.setArrayController.arrangedObjects count]) {
			selectionIndex = 0;
		}
		[self.setArrayController setSelectionIndex:selectionIndex];
	}
	Set	*selected = (Set *)[[self.setArrayController arrangedObjects] objectAtIndex:selectionIndex];
	if (selected) {
		return selected;
	}
	return nil;
}


- (IBAction) playFolderSet:(id)sender
{
	// Disable previous slideshow
	if (slideShow) {
		[slideShow.window close];
		slideShow= nil;
	}
	Set *selected = nil;
	if (sender) {
		NSInteger selectionIndex = [self.setArrayController selectionIndex];
		if (selectionIndex >= 0) {
			selected = (Set *)[[self.setArrayController arrangedObjects] objectAtIndex:selectionIndex];
		}
		if (selected) {
			// update prefernces...
			NSString *sid = selected.setId;
			if (!sid) {
				// for the case when update from the previous data model is required
				sid = [[NSUUID UUID] UUIDString];
				selected.setId = sid;
			}
			prefs.preselectedSetId = selected.setId;
		}
	} else {
		// Set up set from preferences
		selected = [Set getSetById:prefs.preselectedSetId inMoc:self.managedObjectContext];
	}
	if (!selected) {
		//
		// Perhaps user deleted record in the database, but preferences were not updated properly.
		// or show is started via timer.
		//
		NSAlert *alert = [NSAlert alertWithMessageText:@"Cannot find Theme" defaultButton:@"I see" alternateButton:nil otherButton:nil informativeTextWithFormat:@"I cannot find selected theme in database. It is possible. that you deleted theme descri[tion. Please select valid theme from the table on the left."];
		[alert runModal];
		return;
	}
	if (selected && selected.setFolder && selected.setFolder.length > 5) {
		slideShow = [[SlideShowWindowController alloc] initWithParameters:selected];
		[slideShow.window makeKeyWindow];
		[self.window close];
		[slideShow.window makeKeyAndOrderFront:nil];
	} else {
			NSAlert *alert = [NSAlert alertWithMessageText:@"No folder is set" defaultButton:@"I see" alternateButton:nil otherButton:nil informativeTextWithFormat:@"This theme does not have folder with images defined yet. Sorry."];
			[alert runModal];
	}
}

- (IBAction)restoreDataBaseToDefaultState:(id)sender
{
	NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:@"Affirmation"];
	NSError *error = nil;
	NSArray *result = [self.managedObjectContext executeFetchRequest:req error:&error];
	if (!error) {
		for (Affirmation *aff in result) {
			[self.managedObjectContext deleteObject:aff];
		}
		req = [[NSFetchRequest alloc] initWithEntityName:@"Set"];
		result = [self.managedObjectContext executeFetchRequest:req error:&error];
		if (!error) {
			for (Set *aff in result) {
				[self.managedObjectContext deleteObject:aff];
			}
			[self.managedObjectContext save:&error];
			if (error) {
				NSLog(@"error = %@", [error localizedDescription]);
			} else {
				[self createDemoEntry];
			}
		}
	}
}

- (IBAction)wakeTimeChanged:(id)sender
{
	if (sender == self.graphicalTimePicker) {
		self.timePicker.dateValue = self.graphicalTimePicker.dateValue;
	} else {
		self.graphicalTimePicker.dateValue = self.timePicker.dateValue;
	}
	prefs.wakeupTime = self.timePicker.dateValue;
	[self updateWakeupTimer];
}

- (void) updateWakeupTimer
{
	if (prefs.autoWakeup) {
		// enable / reschedule timer
		NSTimeInterval sutki = 24.0*60*60;
		NSDate *today = [NSDate date];
		NSCalendar *gregorian = [NSCalendar currentCalendar];
		NSDateComponents *todayComponents = [gregorian components:VVVallComponents fromDate:today];
		//
		// We should decide when wakeup timer should be fired - today or tomorrow
		//
		NSDateComponents *startComponents = [gregorian components:VVVallComponents fromDate:prefs.wakeupTime];
		[todayComponents setHour:[startComponents hour]];
		[todayComponents setMinute:[startComponents minute]];
		[todayComponents setSecond:[startComponents second]];

		NSDate *startDate = [gregorian dateFromComponents:todayComponents];
		if ([startDate compare:today] != NSOrderedDescending) {
			// We are late... Timer will be fired next day
			NSDate *tmp = [startDate dateByAddingTimeInterval:sutki];
			startDate = tmp;
		}
		if ([wakeupTimer isValid]) {
			// stop old timer!
			[wakeupTimer invalidate];
			wakeupTimer = nil;
		}
		wakeupTimer = [[NSTimer alloc] initWithFireDate:startDate
												  interval: 0 target:self
												selector:@selector(startSlideShow:)
												  userInfo:nil  repeats:NO];
		[[NSRunLoop currentRunLoop] addTimer:wakeupTimer forMode:NSDefaultRunLoopMode];
				
	} else {
		// disable timer;
		if (wakeupTimer && [wakeupTimer isValid]) {
			[wakeupTimer invalidate];
			wakeupTimer = nil;
		}
	}
}

- (void) startSlideShow:(NSTimer *) aTimer
{
	[self playFolderSet:nil];
}


- (IBAction) openSettingswindow:(id)sender
{
	if (slideShow) [slideShow.window close];
	[self restoreMainWindow:nil];
}

- (void) restoreMainWindow:(NSNotification *) note
{
	NSRect frame = [[NSScreen mainScreen] frame];
	NSRect winFrame = self.window.frame;
	winFrame.origin.x = (frame.size.width - winFrame.size.width)*0.5;
	winFrame.origin.y = (frame.size.height - winFrame.size.height) *0.5;
	[self.window setFrame:winFrame display:YES];
	[self.window makeKeyAndOrderFront:self];
}

//
// Reopen closed application window when tap/click on app icon in Dock
//
- (BOOL)applicationShouldHandleReopen:(NSApplication *)theApplication hasVisibleWindows:(BOOL)flag
{
	if (flag == NO) {
		[_window  makeKeyAndOrderFront:self];
	}
	return YES;
}


#pragma mark - ComboBox Delegate

- (NSInteger)numberOfItemsInComboBox:(NSComboBox *)aComboBox
{
	return [self unifiedTextSizes].count;
}

- (id)comboBox:(NSComboBox *)aComboBox objectValueForItemAtIndex:(NSInteger)index
{
	return [[self unifiedTextSizes] objectAtIndex:index];
}


- (NSArray *) unifiedTextSizes
{
	//	NSArray *textSizeArray = [self.setArrayController arrangedObjects];
	
	return @[ @"12", @"14", @"16", @"18", @"20", @"24", @"30", @"40"];
}

- (void)controlTextDidEndEditing:(NSNotification *)notification
{
	NSComboBox * comboBox = [notification object];
	NSString * enteredValue = [comboBox stringValue];
	
	// Don't add blank entry to entities.
	if ([enteredValue length] == 0) return;
	
//	// Pull the entity and context info from the bindings...
//	NSArrayController * contentArrayController = [[comboBox infoForBinding:@"content"]
//												  objectForKey:@"NSObservedObject"];
//	NSString * contentEntityName = [contentArrayController entityName];
//	NSManagedObjectContext * contentContext = [contentArrayController managedObjectContext];
//	NSEntityDescription * contentEntity = [NSEntityDescription entityForName: contentEntityName inManagedObjectContext: contentContext];
//	
//	NSPredicate * predicate = [NSPredicate predicateWithFormat:@"textSize = %@", enteredValue];
//	
//	// Check if this entity already exists...
//	NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
//	[fetch setEntity: contentEntity];
//	[fetch setPredicate: predicate];
//	
//	NSArray * results = [contentContext executeFetchRequest:fetch error:nil];
//	
//	// If not, add it:
//	if (results == nil || [results count] == 0) { NSManagedObject * contentObject = [NSEntityDescription insertNewObjectForEntityForName: contentEntityName inManagedObjectContext: contentContext];
	
//		[contentObject setValue: enteredValue forKey: @"textSize"];
	
		// Refresh the items in the combo box so that we can... (this part is key)
//		[contentContext processPendingChanges];
		[comboBox reloadData];
		
		// ...select the new entry.
		[comboBox setObjectValue: enteredValue];
//	}
}


@end
