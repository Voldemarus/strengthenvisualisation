//
//  Affirmation.m
//  Creative Visualisation
//
//  Created by Водолазкий В.В. on 25.12.14.
//  Copyright (c) 2014 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import "Affirmation.h"
#import "Set.h"

@implementation Affirmation

@dynamic id;
@dynamic text;
@dynamic imageSet;

/*
- (void) awakeFromInsert
{
	// Search for the last id in database
	NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:@"Affirmation"];
	NSSortDescriptor *dsc = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:NO];
	req.sortDescriptors = @[dsc];
	req.fetchLimit = 1;
	NSError *error = nil;
	NSArray *result = [self.managedObjectContext executeFetchRequest:req error:&error];
	if (!result && error) {
		self.id = @(1);
	} else if (result.count == 0) {
		self.id = @(1);
	} else {
		Affirmation *last = [result objectAtIndex:0];
		long long oldId = [last.id longLongValue];
		oldId++;
		self.id = @(oldId);
	}
}
*/

- (NSString *) description
{
	return [NSString stringWithFormat:@"%@ : %@ -> %@", [self className], self.id, self.text];
}

@end
