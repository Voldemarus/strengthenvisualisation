//
//  SlideShowWindowController.h
//  Portal Picture
//
//  Created by Водолазкий В.В. on 31.12.14.
//  Copyright (c) 2014 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Set.h"
#import "Affirmation.h"
#import "Preferences.h"

extern NSString * const VVVrestoreSettingsScreen;

@interface SlideShowWindowController : NSWindowController < NSAlertDelegate>

// Two imageView is actually reuired to make simple interslide transition
@property (nonatomic, retain) IBOutlet NSImageView *slide1;
@property (nonatomic, retain) IBOutlet NSImageView *slide2;
@property (nonatomic, retain) IBOutlet NSView *view;

@property (nonatomic, retain) IBOutlet NSTextField *affirmationText;

- (id) initWithParameters:(Set *)aSet;


- (void) nextSlide;			// force next slide presentation

@end
