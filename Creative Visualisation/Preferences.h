//
//  Preferences.h
//  Portal Picture
//
//  Created by Водолазкий В.В. on 30.12.14.
//  Copyright (c) 2014 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>

#define VVVallComponents (NSYearCalendarUnit | NSMonthCalendarUnit | \
NSDayCalendarUnit | NSWeekCalendarUnit | \
NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit )


//
// These notificators sends variable' states as NSNumber* in [note object]
// to simplify extracting current value of parameter
//
extern NSString * const VVVWakeupChanged;
extern NSString * const VVVForegroundChanged;
extern NSString * const VVVautoWakeupChanged;

@interface Preferences : NSObject

+ (Preferences *) sharedPreferences;
- (void) flush;

@property (nonatomic, readwrite) BOOL firstStart;
@property (nonatomic, retain) NSString *preselectedSetId;

@property (nonatomic, retain) NSDate *wakeupTime;
@property (nonatomic, readwrite) BOOL autoWakeup;
@property (nonatomic, readwrite) BOOL alwaysForeGround;



@end
