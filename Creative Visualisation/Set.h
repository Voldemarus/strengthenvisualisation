//
//  Set.h
//  Creative Visualisation
//
//  Created by Водолазкий В.В. on 25.12.14.
//  Copyright (c) 2014 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Affirmation;

@interface Set : NSManagedObject

@property (nonatomic, retain) NSNumber * playRandom;
@property (nonatomic, retain) NSNumber * playSpeed;
@property (nonatomic, retain) NSNumber * randomPosition;
@property (nonatomic, retain) NSString * setFolder;
@property (nonatomic, retain) NSString * setName;
@property (nonatomic, retain) id textColor;
@property (nonatomic, retain) NSNumber * textPlayRandom;
@property (nonatomic, retain) NSNumber * textPlaySpeed;
@property (nonatomic, retain) NSString * textSize;
@property (nonatomic, retain) NSSet *affirmations;
@property (nonatomic, retain) NSString *setId;

@property (nonatomic, retain) NSNumber *themeId;

@end

@interface Set (CoreDataGeneratedAccessors)

- (void)addAffirmationsObject:(Affirmation *)value;
- (void)removeAffirmationsObject:(Affirmation *)value;
- (void)addAffirmations:(NSSet *)values;
- (void)removeAffirmations:(NSSet *)values;

+ (Set *) getSetById:(NSString *)aId inMoc:(NSManagedObjectContext *)moc;

@end
