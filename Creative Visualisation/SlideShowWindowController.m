//
//  SlideShowWindowController.m
//  Portal Picture
//
//  Created by Водолазкий В.В. on 31.12.14.
//  Copyright (c) 2014 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import "SlideShowWindowController.h"

#import "AppDelegate.h"

NSString * const VVVrestoreSettingsScreen	=	@"VVVrestoreSettingsScreen";

#define CROSSFADE_TRANSITION_TIME	0			// Time to crossfade transition effect




@interface SlideShowWindowController () {
	NSArray *imagesInDirectory;
	NSMutableArray *currentSequence;
	NSInteger currentSlide;
	
	NSMutableArray *affirmSequence;
	NSInteger currentAffirm;
	
	BOOL escKeyPressed;
	
	double timerInterval;
	
	NSTimer *affirmationTimer;
	NSTimer *flashTimer;
	double affirmationTimerInterval;
	
	
	id eventMonitor;				// interceptor for ketboatd events
	BOOL pause;
	
	BOOL  firstSlideIsActive;		// Trigger to decide where new slide should be placed
	
	Set *selectedSet;
	
	Preferences *prefs;
}

@property (nonatomic, retain) NSTimer *changeTimer;			// timer which move to the next slide

@end

@implementation SlideShowWindowController


- (id) initWithParameters:(Set *)aSet
{
	if (self = [super initWithWindowNibName:@"SlideShowWindowController"]) {
		selectedSet = aSet;
		prefs = [Preferences sharedPreferences];
		escKeyPressed = NO;
	}
	return self;
}


- (void) setupSlideShow
{
	self.affirmationText.stringValue = @"";		// clear text label
	self.affirmationText.textColor = selectedSet.textColor;
	float ts = [selectedSet.textSize floatValue];
	if (ts <= 5.0) ts = 5.0;
	self.affirmationText.font = [NSFont fontWithName:@"Arial" size:ts];
	
	self.window.title = selectedSet.setName;
	
	[self setupImagesList];			// generate list of the images for sequential display
	[self setupAffirmationList];	// and list of affirmation texts
	
	currentSlide = 0;
	[self changeSlide:nil];	// display first slide
	[self changeAffirmation:nil];
	//
	// set up slide change interval
	//
	timerInterval = [self getTimerIntervalFor:selectedSet.playSpeed];
	//	NSLog(@"timerInterval = %f",timerInterval);
	
	if (self.changeTimer) {
		[self.changeTimer invalidate];
		self.changeTimer = nil;
	}
	self.changeTimer = [NSTimer scheduledTimerWithTimeInterval:timerInterval target:self
												 selector:@selector(changeSlide:) userInfo:nil repeats:YES];

	// setup priority ABOVE system menu
	if (prefs.alwaysForeGround ) {
		[self.window setLevel:NSMainMenuWindowLevel + 2];
	}
}

/*
- (void)mouseDown:(NSEvent *)theEvent
{
	AppDelegate *d = (AppDelegate *)[[NSApplication sharedApplication] delegate];
	Set *newSet =  [d getNextSet];
	if (newSet) {
		selectedSet = newSet;
		[self setupSlideShow];
	}
}
 */

// Comment line below to show slide show on the full screen
#define SMALL_DEBUG_SLIDE_SHOW	1

- (void)windowDidLoad {
    [super windowDidLoad];
	
	[self setShouldCascadeWindows:NO];
	
	[self.slide1 setAutoresizingMask:NSViewWidthSizable | NSViewHeightSizable];
	[self.slide2 setAutoresizingMask:NSViewWidthSizable | NSViewHeightSizable];
	[self.affirmationText setAutoresizingMask:NSViewWidthSizable | NSViewHeightSizable];
	
	self.window.backgroundColor = [NSColor blackColor];
	NSRect frame = [self.window frame];
	frame.size = NSMakeSize(300.0, 300.0);
	//	[self.window setFrame: frame display: YES animate: NO];
	// resize image to the window's size
	[self.slide1 setFrame:frame];
	[self.slide2 setFrame:frame];
	//	[NSMenu setMenuBarVisible:NO];

	NSRect viFrame =  [[NSScreen mainScreen] visibleFrame];
	// Now position window
	// Dock Menu can ve positioned on the left or right edge
	frame.origin.x =  viFrame.size.width - frame.size.width - viFrame.origin.x;
	frame.origin.y = viFrame.origin.y;
	
	[self.window setFrame:frame display:YES];
	//	NSLog(@"origin -- %.f %.f", frame.origin.x, frame.origin.y);
	
	
	self.window.acceptsMouseMovedEvents = YES;

	[self setupSlideShow];
	
	// set up NSKeyBoard Events
	
	[self.window makeKeyAndOrderFront:nil];
	NSEvent* (^handler)(NSEvent*) = ^(NSEvent *theEvent) {
//		 NSWindow *targetWindow = theEvent.window;
//		DLog(@"window title = %@ --> %d", theEvent.window.title, theEvent.keyCode);
//		   if (targetWindow != self.window) {
//		              return theEvent;
//		          }
		
		          NSEvent *result = theEvent;
//		          DLog(@"event monitor: %@", theEvent);
		          if (theEvent.keyCode == 53) {
		              [self myProcessEscKeyDown];
		              result = nil;
				  }
		      return result;
		};
	  eventMonitor = [NSEvent addLocalMonitorForEventsMatchingMask:NSKeyDownMask handler:handler];

//	// Hide mouse cursor
//	CGDisplayHideCursor (kCGNullDirectDisplay);
}

- (BOOL) windowShouldClose:(NSNotification *)notification
{
	NSLog(@"notification = %@",notification);
	
	CGRect frame = self.window.frame;
	CGRect screenSize = [[NSScreen mainScreen] frame];
	
	if (fabs(frame.size.width - screenSize.size.width)/ screenSize.size.width < 0.1 &&
		fabs(frame.size.height - screenSize.size.height) / screenSize.size.height < 0.1 ) {
		// force quit
		[[NSNotificationCenter defaultCenter] postNotificationName:VVVStopApplication object:nil];
		return YES;
		}
	
	if (escKeyPressed == NO) {
		NSAlert *alert = [NSAlert alertWithMessageText:@"Close application?"
									 defaultButton:@"Close application" alternateButton:@"Return to Settings Window" otherButton:@"Continue"
						 informativeTextWithFormat:@"Please select, what should be done"];
		alert.delegate = self;
		NSModalResponse alertResponse = [alert runModal];
		if (alertResponse == 0) {
			// Return to settings Window
			[self actualClose];
			[[NSNotificationCenter defaultCenter] postNotificationName:VVVrestoreSettingsScreen object:nil];
			return YES;
		} else if (alertResponse == -1) {
			// continue
			return NO;
		} else if (alertResponse == 1) {
			[[NSNotificationCenter defaultCenter] postNotificationName:VVVStopApplication object:nil];
			return YES;
		}
		return NO;
	} else {
		[self actualClose];
		return YES;
	}

}

- (void) actualClose
{
	//	self.window.acceptsMouseMovedEvents = NO;
	CGDisplayShowCursor (kCGNullDirectDisplay);
   if (eventMonitor) [NSEvent removeMonitor:eventMonitor];
	[NSMenu setMenuBarVisible:YES];
}

#pragma mark -

// method scans folder and form list of the images in the alphabetical order
- (void) setupImagesList
{
	NSError *error = nil;
	NSString *folderName = selectedSet.setFolder;
	NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderName error:&error];
	if (error) {
		NSAlert *alert = [NSAlert alertWithError:error];
		[alert runModal];
		[self.window close];
	}
	// get list of all image types supported by current version of Cocoa framework
	NSArray *imageTypes = [NSImage imageFileTypes];
//	DLog(@"imageTypes = %@", imageTypes);
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pathExtension IN %@", imageTypes];
	NSArray *onlyPics = [dirContents filteredArrayUsingPredicate:predicate];
	// now sort data in array in alphabetical order
	imagesInDirectory = [onlyPics sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
	
	currentSequence = nil;
	currentSequence = [[NSMutableArray alloc] initWithCapacity:imagesInDirectory.count];
	[self initSlideArrayWithMode:[selectedSet.playRandom boolValue]];
	
	firstSlideIsActive = NO;	// to start from the first slide' imageView
}

- (void) initSlideArrayWithMode:(BOOL) newMode
{
//	// Stop current playback
//	if (self.changeTimer) {
//		[self.changeTimer invalidate];
//		self.changeTimer = nil;
//	}
	currentSlide = 0;
	[currentSequence removeAllObjects];		// clear all data from array
	if (newMode) {
		// initialize slide show array in the random order
		NSMutableArray *tmpSet = [[NSMutableArray alloc] initWithArray:imagesInDirectory];
		NSInteger count = tmpSet.count;	// fix to avoid crash in enumerator!
		for (NSInteger i = 0; i < count; i++) {
			NSInteger lCount = tmpSet.count;	// Note! We get count for the current tmpSet which is decreasing on each step
			NSInteger rnd = 0;	// last element' index
			if (lCount > 1) {
				rnd = arc4random() % lCount;
			}
			NSString *tmp = [tmpSet objectAtIndex:rnd];
			[tmpSet removeObjectAtIndex:rnd];				// remove selected element from the tmp array
			[currentSequence addObject:tmp];				// and append it to the slideShowArray
		}
		
	} else {
		// initialize slideshow array as a sequential array - sequence is set up by alphabetical order in
		// imagesInDirectory array, so no sorting is required
		[currentSequence addObjectsFromArray:imagesInDirectory];
	}
}

- (void) initAffirmationsWithMode:(BOOL) newMode
{
	[affirmationTimer invalidate];
	affirmationTimer = nil;
	currentAffirm = 0;
	if (affirmSequence) {
		[affirmSequence removeAllObjects];
	} else {
		affirmSequence = [[NSMutableArray alloc] initWithCapacity:selectedSet.affirmations.count];
	}
	if (newMode) {
		// initialize slide show array in the random order
		NSMutableArray *tmpSet = [[NSMutableArray alloc] initWithArray:[selectedSet.affirmations allObjects]];
		NSInteger count = tmpSet.count;	// fix to avoid crash in enumerator!
		for (NSInteger i = 0; i < count; i++) {
			NSInteger lCount = tmpSet.count;	// Note! We get count for the current tmpSet which is decreasing on each step
			NSInteger rnd = 0;	// last element' index
			if (lCount > 1) {
				rnd = arc4random() % lCount;
			}
			NSString *tmp = [tmpSet objectAtIndex:rnd];
			[tmpSet removeObjectAtIndex:rnd];				// remove selected element from the tmp array
			[affirmSequence addObject:tmp];				// and append it to the slideShowArray
		}
		
	} else {
		// initialize slideshow array as a sequential array - sequence is set up by alphabetical order in
		// imagesInDirectory array, so no sorting is required
		[affirmSequence addObjectsFromArray:[selectedSet.affirmations allObjects]];
	}
}

#pragma mark - Affirmations

- (void) setupAffirmationList
{
	if (selectedSet.affirmations.count == 0) return;
	affirmationTimerInterval = [self getTimerIntervalFor:selectedSet.textPlaySpeed];
	
	[self initAffirmationsWithMode:[selectedSet.textPlayRandom boolValue]];
	
	affirmationTimer = [NSTimer scheduledTimerWithTimeInterval:affirmationTimerInterval target:self
													  selector:@selector(changeAffirmation:) userInfo:nil repeats:YES];
}

- (void) changeAffirmation:(NSTimer *)aTimer
{
	if (selectedSet.affirmations.count == 0) return;
	
	Affirmation *aff = [affirmSequence objectAtIndex:currentAffirm];
	
	self.affirmationText.stringValue = aff.text;
	//
	// Adjust size of the control
	//
	[self.affirmationText sizeToFit];
	NSRect frame = self.affirmationText.frame;		// get size of text
	NSRect winFrame = self.window.frame;			// and window itself

//	NSLog(@"affirmation size = %.2f   %.2f", frame.size.width, frame.size.height);
	double aspRatio = frame.size.width / (winFrame.size.width * 0.85);
	if (aspRatio  > 1.0) {
		// This is too wide...
		frame.size.height *= aspRatio*1.5;		// interline interval
		frame.size.width /= aspRatio;
		
//		NSLog(@"Recalc affirmation size = %.2f   %.2f", frame.size.width, frame.size.height);

	}
	
	//
	// Set up position
	//
	float maxX = winFrame.size.width - frame.size.width;
	float maxY = winFrame.size.height - frame.size.height;
	if ([selectedSet.randomPosition boolValue]) {
		// random position on screen
		// We should be sure that this text fir on screen
		frame.origin.x = ((float)rand() / RAND_MAX) * maxX;
		frame.origin.y = ((float)rand() / RAND_MAX) * maxY;
		
//		NSLog(@"x = %.3f y = %.3f", frame.origin.x, frame.origin.y);
	} else {
		// fixed position
		frame.origin.x = maxX * 0.5;
		frame.origin.y = maxY * 0.5;
	}
//	NSLog(@" ");
	[self.affirmationText setFrame:frame];
	self.affirmationText.alphaValue = 1.0;
	flashTimer = [NSTimer scheduledTimerWithTimeInterval:affirmationTimerInterval / 10.0 target:self selector:@selector(hideAffirmation:)
												userInfo:nil repeats:NO];
	
	currentAffirm++;
	if (currentAffirm >= affirmSequence.count) {
//		if ([selectedSet.textPlayRandom boolValue]) {
//			// random position 
			[self setupAffirmationList];
//		}
		currentAffirm = 0;
	}
}

- (void) hideAffirmation:(NSTimer *)aTimer
{
	self.affirmationText.alphaValue = 0.0;
}


#pragma mark - Slides

- (void) nextSlide
{
	[self.changeTimer invalidate];
	self.changeTimer = nil;
	[self changeSlide:nil];
	// Restart timer to preserve proper interval to the next slide
	self.changeTimer = [NSTimer scheduledTimerWithTimeInterval:timerInterval target:self
													 selector:@selector(changeSlide:) userInfo:nil repeats:YES];
}

- (void) changeSlide:(NSTimer *)aTimer
{
	if (aTimer) {
		
		// if pause is set manually - no change slide is called by timer. But in manual mode
		// slide change command will be processed - no special processing is required
		// except sanity check below
		
		if (pause) return;
		
	} else {
		if (currentSlide >=  currentSequence.count || currentSlide < 0) {
			// We get here due to races between timer's thread and event monitor.
			// Therefore we set up sanity check here. It is much simpler than any GCD-based locks
			currentSlide = 0;
		}
	}
	
	NSString *imageName =  [selectedSet.setFolder stringByAppendingPathComponent:[currentSequence objectAtIndex:currentSlide]];
	
	// get image from the folder and tune imageView size to provide proper scaling
	NSImage *image = [[NSImage alloc] initWithContentsOfFile:imageName];
	[self rescaleImageView:image.size];
	
	// put image into proper imageView
	if (firstSlideIsActive) {
		self.slide2.image = image;
	} else {
		self.slide1.image = image;
	}

	// CrossFade transition effect - old slide fades away and new slide fades in
	// in CROSSFADE_TRANSITION_TIME secs
	[NSAnimationContext runAnimationGroup:^(NSAnimationContext *context) {
		
		[context setDuration:CROSSFADE_TRANSITION_TIME];
		[[self.slide1 animator] setAlphaValue:(firstSlideIsActive ? 0.0 : 1.0)];
		[[self.slide2 animator] setAlphaValue:(firstSlideIsActive ? 1.0 : 0.0)];
		
	} completionHandler:^{
		
	}];
	
	// switch trigger to the next step
	firstSlideIsActive = !firstSlideIsActive;
	
	// update slide index, which will be used at the NEXT time
	currentSlide++;
	
	// if we call this selector manually from key event monitor - we just change position of the
	// slideshow index - no reinitalizing is required when all slides have been shown
	
	if (self.changeTimer && currentSlide >= imagesInDirectory.count ) {
		// re initialize sequence
		[self initSlideArrayWithMode:[selectedSet.playRandom boolValue]];
	}
}

//
// Image scaling is performaed in accordance with design guideline
//
- (void) rescaleImageView:(CGSize) imageSize
{

	CGRect imViewFrame = self.window.frame;
	float centerX = imViewFrame.size.width / 2.0;
	
//	// sanity check for the case of empty image file
//	float aspectRatio =  (imageSize.width > 0 ? imageSize.height / imageSize.width : 1.0);
//	if (imageSize.height == 0) aspectRatio = 1.0;
//	
//	BOOL widthIsLesser = YES;
//	float minimalDimension = imViewFrame.size.width;
//	if (imViewFrame.size.height < minimalDimension) {
//		widthIsLesser = NO;
//		minimalDimension = imViewFrame.size.height;
//	}
//	
	float slideWidth = imViewFrame.size.width;
	float slideHeight = imViewFrame.size.height;
	
//	float slideWidth = imViewFrame.size.height / aspectRatio;
	float leftX = centerX - slideWidth / 2.0;
	// Create rectangle for imageView.
	//
	// No need to calculate centerY position due to all extent of parent window' frame is used
	//
	CGRect slideRect = NSMakeRect(leftX, 0.0, slideWidth, slideHeight);
	if (firstSlideIsActive) {
		self.slide2.frame = slideRect;
	} else {
		self.slide1.frame = slideRect;
	}
}

//
// Use simple table concersion due to limited size of choices
//
- (double) getTimerIntervalFor:(NSNumber *)aSpeed
{
	double template[10] = { 4.0, 3.0, 2.0, 1.5, 1.0, 0.75, 0.5, 0.4, 0.3, 0.2 };
	int speedIndex = [aSpeed intValue];
	if (speedIndex < 0 || speedIndex >= 10) return  template[4];
	return template[speedIndex];
}


#pragma mark - Selectors -

- (void) shuffleModeChanged:(NSNotification *) note
{
	BOOL newMode = [[note object] boolValue];
	[self initSlideArrayWithMode:newMode];
}


#pragma mark - Key Event processing methods -

- (void) myProcessEscKeyDown
{
	escKeyPressed = YES;
	[self.changeTimer invalidate];
	self.changeTimer = nil;
	[affirmationTimer invalidate];
	affirmationTimer = nil;
	[self.window close];
	[[NSNotificationCenter defaultCenter] postNotificationName:VVVrestoreSettingsScreen object:nil];
}

- (BOOL)canBecomeKeyWindow {
	return YES;
}

- (BOOL) acceptsFirstResponder
{
	return YES;
}

#pragma mark - Mouse events -

- (void) mouseMoved:(NSEvent *)theEvent
{
	// TODO!  Put timed popover with "End slideshow" button here
}

@end
