//
//  Set.m
//  Creative Visualisation
//
//  Created by Водолазкий В.В. on 25.12.14.
//  Copyright (c) 2014 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "Set.h"
#import "Affirmation.h"

#define TEMPLATE	@"Theme "		// space is mandatory!

@implementation Set

@dynamic playRandom;
@dynamic playSpeed;
@dynamic randomPosition;
@dynamic setFolder;
@dynamic setName;
@dynamic textColor;
@dynamic textPlayRandom;
@dynamic textPlaySpeed;
@dynamic textSize;
@dynamic affirmations;
@dynamic setId;
@dynamic themeId;

- (void) awakeFromInsert
{
	self.textColor = [NSColor redColor];
	self.playSpeed = @(3);
	self.textPlaySpeed = @(3);
	self.textSize = @"12";					// String value to provide compatibility with combobox
	self.playRandom = @(NO);
	self.randomPosition = @(NO);
	
	//
	// Make unique set identifier, used to autoload set when application is started
	//
	self.setId = [[NSUUID UUID] UUIDString];
	
	//
	// Generate new theme id
	//
//	self.setName = [NSString stringWithFormat:@"Theme %ld",(long)[self themeIdentifier]];

	self.setName = @"New Theme";
	
	NSBundle *bundle = [NSBundle mainBundle];
	NSString *pathString = [bundle resourcePath];
	NSString *affirmPath = [bundle pathForResource:@"affirmations-default" ofType:@"txt"];

	NSError *error = nil;
	self.setFolder = pathString;
	self.textPlayRandom = @(NO);
	if (affirmPath) {
		NSString *affirmData = [[NSString alloc] initWithContentsOfFile:affirmPath encoding:NSUTF8StringEncoding error:&error];
		if (error) {
			NSAlert *alert = [NSAlert alertWithError:error];
			[alert runModal];
			return;
		}
		NSArray *affirmStrings = [affirmData componentsSeparatedByString:@"\r"];
		for (NSInteger i = 0; i < affirmStrings.count; i++) {
			NSString *aString = [affirmStrings objectAtIndex:i];
			if (aString.length > 1) {
				NSString *bString = [aString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
				Affirmation *affirm = [NSEntityDescription insertNewObjectForEntityForName:@"Affirmation"
																	inManagedObjectContext:self.managedObjectContext];
				if (affirm) {
					affirm.id = @(i);
					affirm.text = bString;
					affirm.imageSet = self;
				}
//				NSLog(@"%@",affirm);
			}
		}
	}
}

- (NSString *) description
{
	return [NSString stringWithFormat:@"Set %@ : %@  -> %@  / %llu", self.setId, self.setName,
			self.setFolder, (unsigned long long) self.affirmations.count];
}

+ (Set *) getSetById:(NSString *)aId inMoc:(NSManagedObjectContext *)moc
{
	if (!aId) return nil;
	NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:@"Set"];
	req.predicate = [NSPredicate predicateWithFormat:@"setId = %@",aId];
	NSError *error = nil;
	NSArray *result = [moc executeFetchRequest:req error:&error];
	if (!result && error) {
		NSLog(@"Cannot search Set entity for \"%@\" -- %@",  aId, [error localizedDescription]);
		return nil;
	}
	if (result.count == 0) {
		return nil;
	}
	return (Set *) [result objectAtIndex:0];
}


- (NSInteger) themeIdentifier
{
	NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:[self className]];
	req.fetchLimit = 1;
	req.sortDescriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"themeId" ascending:NO]];
	
	NSError *error = nil;
	NSArray *result = [self.managedObjectContext executeFetchRequest:req error:&error];
	if (!result && error) {
		NSLog(@"Error during theme Identifier creation - %@", [error localizedDescription]);
		return 0;
	}
	if (result.count > 0) {
		Set *selected = [result objectAtIndex:0];
		NSInteger oldValue = [selected.themeId integerValue] + 1;
		self.themeId = @(oldValue);
		return oldValue;
	}
	return 0;
}

@end
